#include "painter.h"

Painter::Painter(QGraphicsScene *passedScene, QPixmap *passedBoardImage,
                 QPixmap *passedBishopImage, size_t size):
cellsCount(defaultSize),
boardMap (size, std::vector<bool> (size,false))
{
    scene = passedScene;
    boardImage = passedBoardImage;
    bishopImage = passedBishopImage;
    sceneWidth = scene->width();
    sceneHeight = scene->height();
    tryEllipse = 0;
    Resize(size);
}

Painter::Painter(QObject *parent) : QObject(parent)
{
}

Painter::~Painter()
{

}
void Painter::Resize(int size)
{
   QRect rect(0,
              0,
              boardImage->width()*size/defaultSize,
              boardImage->height()*size/defaultSize );

   littleBoardImage = boardImage->copy(rect);
   littleBoardImage = littleBoardImage.scaled(sceneWidth, sceneHeight);
   cellWidth = sceneWidth/size;
   cellHeight = sceneHeight/size;
   littleBishopImage = bishopImage->scaled(cellWidth, cellHeight);
}

void Painter::Draw()
{
    scene->clear();
    tryEllipse = 0;
    scene->addPixmap(littleBoardImage);
    for(size_t i = 0; i < cellsCount; i++)
        for(size_t j = 0; j < cellsCount; j++)
        {
            if(boardMap[i][j])
            {
                QGraphicsPixmapItem* item = scene->addPixmap(littleBishopImage);
                item->setPos(i*cellWidth, j*cellHeight);

            }
        }
}

void::Painter:: DrawTry(int x, int y)
{   if(tryEllipse )
    {
    scene->removeItem(tryEllipse);
    Draw();
    }
    QBrush gb(Qt::green);
    QPen bp(Qt::black);
    bp.setWidth(1);
    tryEllipse = scene->addEllipse(cellWidth*x, cellHeight*y,cellWidth, cellHeight, bp, gb);
}


void Painter::SetBoardMap(std::vector< std::vector <bool> > passedBoardMap)
{
 boardMap = passedBoardMap;
}

void Painter::PutBishop(size_t x, size_t y)
{
    boardMap[x][y] = true;
}

void Painter::RemoveBishop(size_t x, size_t y)
{
    boardMap[x][y] = false;
}

void Painter::Test1()
{
    qDebug()<<"ddd";
    PutBishop(5,5);
    RemoveBishop(7,1);
    Draw();
}
