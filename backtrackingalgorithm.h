#ifndef BACKTRACKINGALGORITHM_H
#define BACKTRACKINGALGORITHM_H
#include "painter.h"
#include<QPushButton>
#include<QLabel>
#include <QDebug>
#include <vector>

class BackTrackingAlgorithm
{
public:
     BackTrackingAlgorithm(size_t passedBoardSize,
                           size_t passedBishopCount,
                           Painter *passedDrawer,
                           QPushButton *passedContinueButton,
                           QLabel *resultsCountLabel,
                           bool* autoFind);
    ~BackTrackingAlgorithm();
     void Calculate();
 private:
    static const int defaultSize = 8;
    BackTrackingAlgorithm();
    size_t boardSize;
    size_t bishopCount;
    std::vector < std::vector <bool> > boardMap;
    int resultsCount;
    Painter *drawer;
    QPushButton *continueButton;
    QLabel * resultsCountLabel;
    bool* m_autoFind;

    /*
     * true - Можно поставить
     * false - Нельзя т. к. клетка под ударом
     */
    bool TryBishop(int row, int cell);

    void SetBishop(int currentRow, int currentCell,
                   int alreadyBishopsLocated);

};

#endif // BACKTRACKINGALGORITHM_H
