#ifndef PAINTER_H
#define PAINTER_H
#include <QDebug>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QtGui>
#include <vector>
#include <QObject>
class Painter :  public QObject
{
    Q_OBJECT
public:
     explicit Painter(QObject *parent = 0);
     Painter(QGraphicsScene *passedScene,
             QPixmap *passedBoardImage,
             QPixmap *passedBishopImage,
             size_t size);

    ~Painter();
     void SetBoardMap(std::vector< std::vector <bool> > passedBoardMap);
     void Resize(int size);

public slots:
     void Test1();
     void PutBishop(size_t x, size_t y);
     void RemoveBishop(size_t x, size_t y);
     void Draw();
     void DrawTry(int x, int y);
private:
    static const int defaultSize = 8;
    QGraphicsScene *scene;
    QPixmap *boardImage;
    QPixmap *bishopImage;
    QPixmap littleBoardImage;
    QPixmap littleBishopImage;
    int sceneWidth;
    int sceneHeight;
    int cellWidth;
    int cellHeight;
    QGraphicsEllipseItem* tryEllipse;
    int cellsCount;
    std::vector < std::vector <bool> > boardMap;
};

#endif // PAINTER_H
