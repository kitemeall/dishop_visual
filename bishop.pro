#-------------------------------------------------
#
# Project created by QtCreator 2015-03-17T12:35:36
#
#-------------------------------------------------

QT       += gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = bishop
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    painter.cpp \
    backtrackingalgorithm.cpp

HEADERS  += mainwindow.h \
    painter.h \
    backtrackingalgorithm.h

FORMS    += mainwindow.ui

DISTFILES +=

RESOURCES += \
    images.qrc
