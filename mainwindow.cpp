
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    autoFind = new bool;
    *autoFind = 0;

    ui->setupUi(this);
    scene = new QGraphicsScene();
    ui->gvBoard->setScene(scene);

    boardImage = new QPixmap(":images/board.jpg");
    bishopImage = new QPixmap(":images/bishop.png");
    scene->setSceneRect(0, 0, ui->gvBoard->width()-1,ui->gvBoard->height()-1);
    drawer = new Painter(scene,boardImage, bishopImage, 8 );
    drawer->Draw();


}

MainWindow::~MainWindow()
{
    delete scene;
    delete boardImage;
    delete bishopImage;
    delete drawer;
    if(backTracking!=0)
            delete backTracking;
    delete autoFind;
    delete ui;
}


void MainWindow::on_pbStart_clicked()
{
    ui->pbStart->setEnabled(false);
    ui->sbBishopCount->setEnabled(false);
    ui->sbBoardSize->setEnabled(false);
    size_t boardSize = ui->sbBoardSize->value();
    size_t bishopCount = ui->sbBishopCount->value();
    backTracking = new BackTrackingAlgorithm(boardSize, bishopCount,
                                             drawer,ui->pbNext,
                                             ui->lResultCount,
                                             autoFind);
    backTracking->Calculate();
}

void MainWindow::on_sbBoardSize_valueChanged(int val)
{
    ui->sbBishopCount->setMaximum(val*val);
}

void MainWindow::on_checkBox_clicked(bool checked)
{
    *autoFind = checked;
    qDebug() <<*autoFind;
}
