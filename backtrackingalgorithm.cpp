#include "backtrackingalgorithm.h"

BackTrackingAlgorithm::BackTrackingAlgorithm(size_t passedBoardSize,
                                             size_t passedBishopCount,
                                             Painter *passedDrawer,
                                             QPushButton *passedContinueButton,
                                             QLabel *passedResultCountLabel,
                                             bool *autoFind):
    boardMap (passedBoardSize, std::vector<bool> (passedBoardSize,false))
{
    drawer = passedDrawer;
    drawer->Resize(passedBoardSize);
    continueButton = passedContinueButton;
    boardSize = passedBoardSize;
    bishopCount = passedBishopCount;
    resultsCount = 0;
    resultsCountLabel = passedResultCountLabel;
    m_autoFind = autoFind;
}

BackTrackingAlgorithm::~BackTrackingAlgorithm()
{

}

bool BackTrackingAlgorithm::TryBishop(int a, int b)
{
    for (int i = 1; i <= a && b - i >= 0; ++i)
    {
        if (boardMap[a - i][b - i])
        {
            return false;
        }
    }

    for (int i = 1; i <= a && b + i < boardSize; i++)
    {
        if (boardMap[a - i][b + i])
        {
            return false;
        }
    }

    return true;
}

void BackTrackingAlgorithm::SetBishop(int currentRow, int currentCell,
                                      int alreadyBishopsLocated)
{
    if (alreadyBishopsLocated == bishopCount)
    {
        drawer->SetBoardMap(boardMap);
        drawer->Draw();
        ++resultsCount;

        resultsCountLabel->setText(QString::number(resultsCount));
        QEventLoop loop;
        QObject::connect(continueButton, SIGNAL(clicked()), &loop, SLOT(quit()));
        loop.exec();

        /*
         * oooo
         * yeah!
         */
        return;
    }


    drawer->SetBoardMap(boardMap);
    drawer->Draw();

    if(!(*m_autoFind))
    {
        QEventLoop loop;
        QObject::connect(continueButton, SIGNAL(clicked()), &loop, SLOT(quit()));
        loop.exec();
    }
    int i = currentRow;
    int j = currentCell+1;
    while (i < boardSize)
    {
        while (j < boardSize)
        {
            if(!(*m_autoFind))
            {
                drawer->DrawTry(i,j);
                QEventLoop loop;
                QObject::connect(continueButton, SIGNAL(clicked()), &loop, SLOT(quit()));
                loop.exec();
            }
            if (TryBishop(i, j))
            {
                boardMap[i][j] = true;
                SetBishop(i, j, alreadyBishopsLocated + 1);
                boardMap[i][j] = false;
            }
            ++j;
        }
        ++i;
        j=0;
    }

    return; // Опционально.
}

void BackTrackingAlgorithm::Calculate()
{
    SetBishop(0, -1, 0);
}
