#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "painter.h"
#include "backtrackingalgorithm.h"
#include <QDebug>
#include <QMainWindow>
#include <QGraphicsScene>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_pbStart_clicked();

    void on_sbBoardSize_valueChanged(int arg1);



    void on_checkBox_clicked(bool checked);

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    QPixmap *boardImage;
    QPixmap *bishopImage;
    Painter *drawer;
    BackTrackingAlgorithm *backTracking;
    bool *autoFind;
};

#endif // MAINWINDOW_H
